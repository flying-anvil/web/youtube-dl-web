CREATE TABLE `playlists`
(
    `playlist_id` varchar(36)  NOT NULL,
    `name`        varchar(255) NOT NULL,
    PRIMARY KEY (`playlist_id`),
    UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
