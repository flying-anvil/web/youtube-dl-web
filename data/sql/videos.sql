CREATE TABLE `videos`
(
    `video_id`    varchar(16)  NOT NULL,
    `video_name`  varchar(255) NOT NULL,
    `playlist_id` varchar(36)  DEFAULT NULL,
    `downloaded`  datetime     DEFAULT NULL,
    `local_path`  varchar(511) DEFAULT NULL,
    PRIMARY KEY (`video_id`, `playlist_id`),
    FOREIGN KEY (`playlist_id`) REFERENCES playlists (playlist_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
