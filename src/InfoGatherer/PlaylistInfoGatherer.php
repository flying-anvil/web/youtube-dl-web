<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\InfoGatherer;

use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistEntries;
use FlyingAnvil\YoutubeDlWeb\DataObject\YoutubeVideo;
use Symfony\Component\Process\Process;

class PlaylistInfoGatherer
{
    public function getPlaylistName(string $playlist): string
    {
        $page = file_get_contents($playlist);

        // <title>My Playlist - YouTube</title>
        preg_match('#<title>(.+)( - YouTube)</title>#', $page, $matches);

        [, $title] = $matches;

        return $title;
    }

    public function getPlaylistEntries(string $playlist): PlaylistEntries
    {
        $process = new Process(['youtube-dl', '--flat-playlist', $playlist, '-j']);
        $process->run();

        if ($process->getErrorOutput() !== '') {
            // TODO: Better Exception
            throw new \Exception('Error getting Playlist Info: ' . $process->getErrorOutput());
        }

        $entries = PlaylistEntries::createEmpty();

        $lines = explode(PHP_EOL, trim($process->getOutput()));
        foreach ($lines as $line) {
            $parsed = json_decode($line, true, 512, JSON_THROW_ON_ERROR);
            $entries->add(YoutubeVideo::create(
                $parsed['id'],
                trim($parsed['title']),
            ));
        }

        return $entries;
    }
}
