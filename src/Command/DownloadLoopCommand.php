<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadLoopCommand extends Command
{
    public const COMMAND_NAME = 'download:loop';

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
    }

    // TODO: query all non-downloaded Videos
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // TODO: Extract Loop into libfa
        while (true) {
            echo 'sleepi\'n', PHP_EOL;
            sleep(60);
        }

        return 0;
    }
}
