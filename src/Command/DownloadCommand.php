<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Command;

use FlyingAnvil\YoutubeDlWeb\Repository\PlaylistRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadCommand extends Command
{
    public const COMMAND_NAME = 'download:missing';

    private PlaylistRepository $playlistRepository;

    public function __construct(PlaylistRepository $playlistRepository)
    {
        $this->playlistRepository = $playlistRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->playlistRepository->getUnfinishedVideos();

        return 0;
    }
}
