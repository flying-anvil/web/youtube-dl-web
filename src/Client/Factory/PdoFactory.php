<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Client\Factory;

use FlyingAnvil\Libfa\Repository\EnvironmentRepository;
use PDO;
use Psr\Container\ContainerInterface;

class PdoFactory
{
    private const ENV_MYSQL_HOST     = 'MYSQL_HOST';
    private const ENV_MYSQL_DATABASE = 'MYSQL_DATABASE';
    private const ENV_MYSQL_USER     = 'MYSQL_USER';
    private const ENV_MYSQL_PASSWORD = 'MYSQL_PASSWORD';

    public function __invoke(ContainerInterface $container): PDO
    {
        $envRepository = $container->get(EnvironmentRepository::class);

        $dsn = sprintf(
            'mysql:host=%s;dbname=%s',
            $envRepository->getEnvironmentVariable(self::ENV_MYSQL_HOST),
            $envRepository->getEnvironmentVariable(self::ENV_MYSQL_DATABASE),
        );

        $user     = $envRepository->getEnvironmentVariable(self::ENV_MYSQL_USER);
        $password = $envRepository->getEnvironmentVariable(self::ENV_MYSQL_PASSWORD);

        return new PDO($dsn, $user, $password, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]);
    }
}
