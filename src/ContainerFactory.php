<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb;

use DI\Container;
use DI\ContainerBuilder;
use FlyingAnvil\YoutubeDlWeb\Client\Factory\PdoFactory;
use PDO;
use function DI\factory;

final class ContainerFactory
{
    public static function create(): Container
    {
        $containerBuilder = new ContainerBuilder();

//        if (PHP_DI_CACHE === true) {
//            $containerBuilder->enableCompilation(__DIR__ . '/../../../data/cache/php-di/', 'YadoreApi_CompiledContainer');
//            $containerBuilder->writeProxiesToFile(true, __DIR__ . '/../../../data/proxies/php-di/yadore-api/');
//        }

        $containerBuilder->addDefinitions([
            PDO::class => factory(PdoFactory::class),
        ]);

        return $containerBuilder->build();
    }
}
