<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\DataObject;

use FlyingAnvil\Fileinfo\Conversion\Stringifyable;
use FlyingAnvil\Fileinfo\DataObject\DataObject;

final class PlaylistId implements DataObject, Stringifyable
{
    private string $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function create(string $id): self
    {
        return new self($id);
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        return $this->id;
    }
}
