<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\DataObject;

use FlyingAnvil\Libfa\Wrapper\File;

final class CachedVideoFile
{
    private VideoId $videoId;
    private string $name;
    private File $file;

    private function __construct(VideoId $videoId, string $name, File $file)
    {
        $this->videoId = $videoId;
        $this->name    = $name;
        $this->file    = $file;
    }

    public static function create(VideoId $videoId, string $name, File $file): self
    {
        return new self($videoId, $name, $file);
    }

    public function getVideoId(): VideoId
    {
        return $this->videoId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFile(): File
    {
        return $this->file;
    }
}
