<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\DataObject;

use FlyingAnvil\Fileinfo\Conversion\Stringifyable;
use FlyingAnvil\Fileinfo\DataObject\DataObject;

final class YoutubeVideo implements DataObject, Stringifyable
{
    private VideoId $id;
    private string $title;
    private ?PlaylistId $playlistId;

    private function __construct(VideoId $id, string $title, PlaylistId $playlist = null)
    {
        $this->id         = $id;
        $this->title      = $title;
        $this->playlistId = $playlist;
    }

    public static function create(VideoId $id, string $title = '', PlaylistId $playlistId = null): self
    {
        return new self($id, $title, null);
    }

    public function getId(): VideoId
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPlaylistId(): ?PlaylistId
    {
        return $this->playlistId;
    }

    public function getUrl(): string
    {
        return sprintf(
            'https://www.youtube.com/watch?v=%s',
            $this->id,
        );
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function jsonSerialize()
    {
        return [
            'id'       => $this->id,
            'title'    => $this->title,
            'playlist' => $this->playlistId,
        ];
    }
}
