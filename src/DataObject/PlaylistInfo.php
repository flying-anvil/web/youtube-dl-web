<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\DataObject;

use FlyingAnvil\Fileinfo\DataObject\DataObject;

final class PlaylistInfo implements DataObject
{
    private PlaylistId $playlistId;
    private string $playlistName;
    private PlaylistEntries $playlistEntries;

    private function __construct(PlaylistId $playlistId, string $playlistName, PlaylistEntries $playlistEntries)
    {
        $this->playlistId      = $playlistId;
        $this->playlistEntries = $playlistEntries;
        $this->playlistName    = $playlistName;
    }

    public static function create(PlaylistId $playlistId, string $playlistName, PlaylistEntries $playlistEntries): self
    {
        return new self($playlistId, $playlistName, $playlistEntries);
    }

    public function getPlaylistId(): PlaylistId
    {
        return $this->playlistId;
    }

    public function getPlaylistEntries(): PlaylistEntries
    {
        return $this->playlistEntries;
    }

    public function getPlaylistName(): string
    {
        return $this->playlistName;
    }

    public function jsonSerialize()
    {
        return [
            'playlistId'      => $this->playlistId,
            'playlistName'    => $this->playlistName,
            'playlistEntries' => $this->playlistEntries,
        ];
    }
}
