<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\DataObject;

use Countable;
use FlyingAnvil\Fileinfo\DataObject\DataObject;
use Generator;
use IteratorAggregate;

final class PlaylistEntries implements DataObject, IteratorAggregate, Countable
{
    /** @var YoutubeVideo[] */
    private array $entries = [];

    private function __construct()
    {
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public function add(YoutubeVideo $entry): void
    {
        $this->entries[] = $entry;
    }

    /**
     * @return Generator<YoutubeVideo> | YoutubeVideo[]
     */
    public function getIterator(): Generator
    {
        yield from $this->entries;
    }

    public function jsonSerialize(): array
    {
        return $this->entries;
    }

    public function count(): int
    {
        return count($this->entries);
    }
}
