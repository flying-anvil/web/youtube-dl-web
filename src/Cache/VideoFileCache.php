<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Cache;

use FlyingAnvil\Libfa\Repository\EnvironmentRepository;
use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\YoutubeDlWeb\DataObject\CachedVideoFile;
use FlyingAnvil\YoutubeDlWeb\DataObject\VideoId;

class VideoFileCache
{
    private const ENV_FILE_SAVE_DIRECTORY = 'FILE_SAVE_DIRECTORY';
    private const DEFAULT_FILE_SAVE_DIRECTORY = '/tmp/yt-cache/';

    private string $cacheDirectory;

    public function __construct(EnvironmentRepository $environmentRepository)
    {
        $cacheDirectory = $environmentRepository->getEnvironmentVariable(
            self::ENV_FILE_SAVE_DIRECTORY,
            self::DEFAULT_FILE_SAVE_DIRECTORY,
        );

        $this->cacheDirectory = rtrim($cacheDirectory, '/') . '/cache';
    }

    public function getFileById(VideoId $videoId): ?CachedVideoFile
    {
        $cache = sprintf('%s/%s/',
            $this->cacheDirectory,
            $videoId->toString(),
        );

        if (is_dir($cache)) {
            $nameFilePath = $cache . '/name.txt';
            $filePath     = $cache . '/video.mp4';

            if (!file_exists($nameFilePath) || !file_exists($filePath)) {
                // TODO: remove directory
                return null;
            }

            $name = File::load($nameFilePath)->readAll();
            $file = File::load($filePath);

            return CachedVideoFile::create(
                $videoId,
                $name,
                $file,
            );
        }

        return null;
    }

    public function putVideo(CachedVideoFile $cachedVideo): CachedVideoFile
    {
        $cache = sprintf('%s/%s/',
            $this->cacheDirectory,
            $cachedVideo->getVideoId()->toString(),
        );

        if (!is_dir($cache) && !mkdir($cache, 0777, true) && !is_dir($cache)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $cache));
        }

        $name = File::load($cache . 'name.txt');
        $name->open('wb');
        $name->write($cachedVideo->getName());
        $cachedVideo->getFile()->copy($cache . 'video.mp4');

        return CachedVideoFile::create(
            $cachedVideo->getVideoId(),
            $cachedVideo->getName(),
            File::load($cache . 'video.mp4'),
        );
    }
}
