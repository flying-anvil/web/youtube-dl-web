<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Api\Playlist\Action;

use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistId;
use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistInfo;
use FlyingAnvil\YoutubeDlWeb\Api\Playlist\InfoGatherer\PlaylistInfoGatherer;
use FlyingAnvil\YoutubeDlWeb\Repository\PlaylistRepository;
use FlyingAnvil\YoutubeDlWeb\Slim\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;

class CreatePlaylistAction implements RequestHandlerInterface
{
    private PlaylistInfoGatherer $infoGatherer;
    private PlaylistRepository $playlistRepository;

    public function __construct(PlaylistInfoGatherer $playlistInfoGatherer, PlaylistRepository $playlistRepository)
    {
        $this->infoGatherer       = $playlistInfoGatherer;
        $this->playlistRepository = $playlistRepository;
    }

    public function __invoke(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $params = $request->getQueryParams();

        // TODO: Error Handling!
        $playlist = $params['playlist'];
        $playlistParams = substr($playlist, strpos($playlist, '?') + 1);

        parse_str($playlistParams, $playlistParams);

        $playlistInfo = PlaylistInfo::create(
            PlaylistId::create($playlistParams['list']),
            $this->infoGatherer->getPlaylistName($playlist),
            $this->infoGatherer->getPlaylistEntries($playlist),
        );

        $this->playlistRepository->createPlaylist(
            $playlistInfo->getPlaylistId(),
            $playlistInfo->getPlaylistName(),
        );

        $enqueuedTitles = $this->playlistRepository->enqueuePlaylist($playlistInfo);

        return $response->withJson([
            'status' => 'ok',
            'count'  => $enqueuedTitles,
        ]);
    }
}
