<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Api\Playlist\Action;

use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistId;
use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistInfo;
use FlyingAnvil\YoutubeDlWeb\Downloader\PlaylistDownloader;
use FlyingAnvil\YoutubeDlWeb\InfoGatherer\PlaylistInfoGatherer;
use FlyingAnvil\YoutubeDlWeb\Slim\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use YoutubeDl\Options;

class DownloadPlaylistAction implements RequestHandlerInterface
{
    private PlaylistInfoGatherer $infoGatherer;
    private PlaylistDownloader $playlistDownloader;

    public function __construct(PlaylistInfoGatherer $playlistInfoGatherer, PlaylistDownloader $playlistDownloader)
    {
        $this->infoGatherer = $playlistInfoGatherer;
        $this->playlistDownloader   = $playlistDownloader;
    }

    public function __invoke(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $params = $request->getQueryParams();

        // TODO: Error Handling!
        $playlist = $params['playlist'];
        $playlistParams = substr($playlist, strpos($playlist, '?') + 1);

        parse_str($playlistParams, $playlistParams);
        $playlistId = PlaylistId::create($playlistParams['list']);

        $playlistInfo = PlaylistInfo::create(
            $playlistId,
            $this->infoGatherer->getPlaylistName($playlist),
            $this->infoGatherer->getPlaylistEntries($playlist),
        );

        die;
        $this->playlistDownloader->downloadPlaylist($playlistInfo->getPlaylistEntries(), Options::create());

        return $response->withJson($playlistInfo);
    }
}
