<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Api\Video\Action;

use FlyingAnvil\Libfa\Wrapper\File;
use FlyingAnvil\YoutubeDlWeb\Cache\VideoFileCache;
use FlyingAnvil\YoutubeDlWeb\DataObject\CachedVideoFile;
use FlyingAnvil\YoutubeDlWeb\DataObject\VideoId;
use FlyingAnvil\YoutubeDlWeb\Downloader\VideoDownloader;
use FlyingAnvil\YoutubeDlWeb\Slim\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use YoutubeDl\Options;

class DownloadVideoAction implements RequestHandlerInterface
{
    private VideoDownloader $videoDownloader;
    private VideoFileCache $videoFileCache;

    public function __construct(VideoDownloader $videoDownloader, VideoFileCache $videoFileCache)
    {
        $this->videoDownloader = $videoDownloader;
        $this->videoFileCache = $videoFileCache;
    }

    public function __invoke(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $params  = $request->getQueryParams();
        $videoId = $params['video'] ?? null;

        if (!$videoId) {
            return $response->withJson([
                'error' => [
                    'message' => 'No video given',
                ],
            ]);
        }

        $video = $this->getVideo(VideoId::create($videoId));

        return $response
            ->withHeader('Content-Disposition', sprintf('filename="%s"', $video->getName()))
            ->withFile($video->getFile()->getFilePath());
    }

    public function getVideo(VideoId $videoId): CachedVideoFile
    {
        $cachedVideo = $this->videoFileCache->getFileById($videoId);

        if ($cachedVideo !== null) {
            return $cachedVideo;
        }

        $video         = $this->videoDownloader->downloadByVideoId($videoId, Options::create());
        $videoFilePath = $video->getFile()->getPathname();

        $videoName = sprintf('%s.%s',
            str_replace(
                ['"',  "\n", "\r"],
                ['\'', '',   ''], $video->getTitle()),
            $video->getExt(),
        );

        $cachedVideo = CachedVideoFile::create(
            $videoId,
            $videoName,
            File::load($videoFilePath),
        );

        return $this->videoFileCache->putVideo($cachedVideo);
    }
}
