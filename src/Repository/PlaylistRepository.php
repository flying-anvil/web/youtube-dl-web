<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Repository;

use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistEntries;
use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistId;
use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistInfo;
use FlyingAnvil\YoutubeDlWeb\DataObject\YoutubeVideo;
use PDO;

class PlaylistRepository
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function createPlaylist(PlaylistId $playlistId, string $playlistName): void
    {
        $sql = <<< SQL
            INSERT IGNORE INTO playlists (playlist_id, name) 
            VALUES (:playlistId, :playlistName)  
        SQL;

        $statement = $this->pdo->prepare($sql, []);
        $statement->execute([
            'playlistId'   => $playlistId->toString(),
            'playlistName' => $playlistName,
        ]);
    }

    // TODO: Maybe query all of playlist and filter in php, returning enqueued titles instead of count
    public function enqueuePlaylist(PlaylistInfo $playlistInfo): int
    {
        $values = [];

        foreach ($playlistInfo->getPlaylistEntries() as $video) {
            $values[] = sprintf(
                '(\'%s\', \'%s\', \'%s\')',
                $video->getTitle(),
                $video->getId(),
                $playlistInfo->getPlaylistId(),
            );
        }

        $sql = <<< SQL
            INSERT IGNORE INTO videos (video_name, video_id, playlist_id)
            VALUES %s
        SQL;

        $sql = sprintf($sql, implode(',' . PHP_EOL, $values));
        $statement = $this->pdo->prepare($sql, []);
        $statement->execute([]);

        return $statement->rowCount();
    }

    public function getUnfinishedVideos(): PlaylistInfo
    {
        $sql = <<< SQL
            SELECT v.video_id, v.video_name, p.playlist_id, p.name FROM videos v
            INNER JOIN playlists p on v.playlist_id = p.playlist_id
        SQL;

        $statement = $this->pdo->prepare($sql, []);
        $statement->execute([]);

        $entries = PlaylistEntries::createEmpty();

        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
            $video = YoutubeVideo::create(
                $row['video_id'],
                $row['video_name'],
                $row['playlist_id'],
            );

            $entries->add($video);
        }

        return PlaylistInfo::create(
            '',

        );
    }
}
