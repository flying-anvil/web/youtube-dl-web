<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Slim;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;

interface RequestHandlerInterface
{
    public function __invoke(ServerRequestInterface $request, Response $response): ResponseInterface;
}
