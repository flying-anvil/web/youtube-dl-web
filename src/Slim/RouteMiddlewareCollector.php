<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Slim;

use FlyingAnvil\YoutubeDlWeb\Api\Playlist\Action\DownloadPlaylistAction;
use FlyingAnvil\YoutubeDlWeb\Api\Playlist\Action\CreatePlaylistAction;
use FlyingAnvil\YoutubeDlWeb\Api\Playlist\Action\PlaylistInfoAction;
use FlyingAnvil\YoutubeDlWeb\Api\Video\Action\DownloadVideoAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

final class RouteMiddlewareCollector
{
    public function register(App $app): void
    {
        $this->registerApiRoutes($app);
//        $this->registerNotFoundRoutes($app);
        $this->registerErrorMiddleware($app);
    }

    private function registerApiRoutes(App $app): void
    {
        $app->get('/test', fn() => 'test');

        $app->group('/api', function (RouteCollectorProxy $group) {
            $routes = [
                $group->get('/playlist/info', PlaylistInfoAction::class)->setName('api-playlist-info'),
                $group->get('/playlist/download', DownloadPlaylistAction::class)->setName('api-playlist-download'),
                $group->get('/playlist/create', CreatePlaylistAction::class)->setName('api-playlist-create'),

                $group->get('/video/download', DownloadVideoAction::class)->setName('api-playlist-create'),
            ];

            $middlewares = array_reverse([]);

            foreach ($routes as $route) {
                foreach ($middlewares as $middleware) {
                    $route->add($middleware);
                }
            }
        });
    }

    public function registerNotFoundRoutes(App $app): void
    {
        $callback = function (ServerRequestInterface $request, ResponseInterface $response) {
            $content404 = '404 - Not Found';
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write($content404);
        };

        $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', $callback);
    }

    private function registerErrorMiddleware(App $app)
    {
        $errorMiddleware = $app->addErrorMiddleware(true, true, true);
    }
}
