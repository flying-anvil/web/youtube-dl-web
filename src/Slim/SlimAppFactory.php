<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Slim;

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;

final class SlimAppFactory
{
    public static function create(ContainerInterface $container): App
    {
        $app = AppFactory::createFromContainer($container);

        $middleWareCollector = new RouteMiddlewareCollector();
        $middleWareCollector->register($app);

        return $app;
    }
}
