<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Wrapper;

class PhpNativeFunction
{
    public function shellExec(string $command): ?string
    {
        return shell_exec($command);
    }
}
