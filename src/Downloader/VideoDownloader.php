<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Downloader;

use FlyingAnvil\YoutubeDlWeb\DataObject\VideoId;
use FlyingAnvil\YoutubeDlWeb\DataObject\YoutubeVideo;
use YoutubeDl\Entity\Video;
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;

class VideoDownloader
{
    private YoutubeDl $downloader;

    public function __construct(YoutubeDl $downloader)
    {
        $this->downloader = $downloader;
    }

    public function downloadByVideoId(VideoId $videoId, Options $options): Video
    {
        $options = $options->url(YoutubeVideo::create($videoId)->getUrl());
        $options = $options->audioQuality('0');
        $options = $options->format('bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio');
        $options = $options->mergeOutputFormat('mp4');
        $result  = $this->downloader->download($options);

        /** @var Video $downloadedVideo */
        $downloadedVideo = $result->get('0');
        return $downloadedVideo;
    }

    public function downloadVideo(YoutubeVideo $video, Options $options): void
    {
        $options = $options->url($video->getUrl());
        $result  = $this->downloader->download($options);

        ini_set('xdebug.var_display_max_depth', '10');
        ini_set('xdebug.var_display_max_children', '256');
        ini_set('xdebug.var_display_max_data', '1024');

        var_dump($result);
    }
}
