<?php

declare(strict_types=1);

namespace FlyingAnvil\YoutubeDlWeb\Downloader;

use FlyingAnvil\YoutubeDlWeb\DataObject\PlaylistEntries;
use YoutubeDl\Options;

class PlaylistDownloader
{
    private VideoDownloader $videoDownloader;

    public function __construct(VideoDownloader $videoDownloader)
    {
        $this->videoDownloader = $videoDownloader;
    }

    public function downloadPlaylist(PlaylistEntries $playlist, Options $options): void
    {
        foreach ($playlist as $video) {
            $this->videoDownloader->downloadVideo($video, $options);
            break;
        }
    }
}
