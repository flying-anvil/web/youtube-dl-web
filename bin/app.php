<?php

declare(strict_types=1);

use FlyingAnvil\YoutubeDlWeb\Command\DownloadCommand;
use FlyingAnvil\YoutubeDlWeb\Command\DownloadLoopCommand;
use FlyingAnvil\YoutubeDlWeb\ContainerFactory;
use FlyingAnvil\YoutubeDlWeb\Slim\SlimAppFactory;
use Symfony\Component\Console\Application as CliApplication;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

(static function () {
    require_once __DIR__ . '/../vendor/autoload.php';

    chdir(dirname(__DIR__));

    $container = ContainerFactory::create();

    if (strpos(PHP_SAPI, 'fpm') === false) {
        $commandLoader = new ContainerCommandLoader($container, [
            DownloadCommand::COMMAND_NAME     => DownloadCommand::class,
            DownloadLoopCommand::COMMAND_NAME => DownloadLoopCommand::class,
        ]);

        $app = new CliApplication();
        $app->setCommandLoader($commandLoader);
        $app->run();

        return;
    }

    $app = SlimAppFactory::create($container);
    $app->run();
})();
